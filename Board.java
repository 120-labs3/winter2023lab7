public class Board{
	
	private Square[][] tictactoeBoard;
	
	public Board(){
		Square blank = Square.BLANK;
		this.tictactoeBoard = new Square[3][3];
		for(int i=0;i<this.tictactoeBoard.length;i++){
			for(int j=0;j<this.tictactoeBoard[i].length;j++){
				
				this.tictactoeBoard[i][j] = blank;
			}
		}
	}
	public String toString(){
		String values ="";
		for(int i=0;i<this.tictactoeBoard.length;i++){
			values += "\n";
			for(int j=0;j<this.tictactoeBoard[i].length;j++){
				
				values += this.tictactoeBoard[i][j];
			}
		}
		return values;
		
	}
	
	public boolean placeToken(int row, int col, Square playerToken){
		if(row > this.tictactoeBoard.length -1 || row < 0 || col >this.tictactoeBoard.length -1 || col <0){
			return false;
		}
		if(this.tictactoeBoard[row][col] == Square.BLANK){
			this.tictactoeBoard[row][col] = playerToken;
			return true;
		}
		return false;
	}
	public boolean checkIfFull(){
		for(int i=0;i<this.tictactoeBoard.length;i++){
			for(int j=0;j<this.tictactoeBoard[i].length;j++){
				if(this.tictactoeBoard[i][j] == Square.BLANK){
					return false;
				}
			}
		}
		return true;
	}
	private boolean checkIfWinningHorizontal(Square playerToken){
		for(int i=0;i<this.tictactoeBoard.length;i++){
			for(int j=0;j<this.tictactoeBoard[i].length;j++){
				if(this.tictactoeBoard[i][j] == playerToken && this.tictactoeBoard[i][j+1] == playerToken && this.tictactoeBoard[i][j+2] == playerToken){
					return true;
			}
			else
				break;		
		}
	}
		return false;
	}
	private boolean checkIfWinningVertical(Square playerToken){
		for(int i=0;i<this.tictactoeBoard.length;i++){
			if(this.tictactoeBoard[i][i] == playerToken && this.tictactoeBoard[i+1][i] == playerToken && this.tictactoeBoard[i+2][i] == playerToken){
				return true;
			}
			else
				break;
		}
		return false;
	}	
	private boolean checkIfWinningDiagonal(Square playerToken){
		for(int i=0;i<this.tictactoeBoard.length;i++){
			if(this.tictactoeBoard[i][i] == playerToken && this.tictactoeBoard[i+1][i+1] == playerToken && this.tictactoeBoard[i+2][i+2] == playerToken){
					return true;
			}
			else if(this.tictactoeBoard[i][i+2] == playerToken && this.tictactoeBoard[i+1][i+1] == playerToken && this.tictactoeBoard[i+2][i] == playerToken){
				return true;
			}
			else
				break;
		}
		return false;
	}
	public boolean checkIfWinning(Square playerToken){
		
		if(checkIfWinningHorizontal(playerToken) || checkIfWinningVertical(playerToken) || checkIfWinningDiagonal(playerToken)){
			return true;
		}
		return false;
	}
	
}