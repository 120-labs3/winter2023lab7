import java.util.Scanner;
public class TikTakToe{
	public static void main(String[] args){

		System.out.println("WELCOME PLAYER!");
		Board board = new Board();
		
		Scanner input = new Scanner(System.in);
		boolean gameOver = false;
		int player = 1;
		Square playerToken = Square.X;
		int row = 0;
		int col = 0;
		
		while(!gameOver){
			boolean tokenValue = true;
			System.out.println(board);
			
			if(player == 1){
				playerToken = Square.X;
			}
			else{
				playerToken = Square.O;
			}
			
			row = 0;
			col = 0;
			while (tokenValue){ //Validates the entered values
				System.out.println("Enter the row");
				row = input.nextInt() -1;
				System.out.println("Enter the Column");
				col = input.nextInt() -1;
				
				if(!(board.placeToken(row,col,playerToken))){ //If it is false
					System.out.println("Please enter a valid value from 1 - 3 that is not already taken");
				}
				else{
					tokenValue = false;
				}
			}
			
			board.placeToken(row,col,playerToken);
			
			
			if(board.checkIfWinning(playerToken)){
				System.out.println(board);
				System.out.println("Player " + player + " is the Winner!!!");
				gameOver = true;
			}
			else if(board.checkIfFull()){
				System.out.println(board);
				System.out.println("It's a tie!");
				gameOver = true;
			}
			else {
				player++;
				if(player > 2){
					player = 1;
				}
			}
		}
	}
}